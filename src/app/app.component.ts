import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/from';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  numbers = [1, 5, 10];

  ngOnInit() {

    let source = Observable.fromEvent(document, 'mouseover')
                          .map( (event: MouseEvent) => {
                            return {
                              x: event.clientX,
                              y: event.clientY
                            };
                          })
                          .filter(value => value.x > 300);

    source.subscribe(
      value => console.log(value),
      e => console.log(`error: ${e}`),
      () => console.log('complete')
    );
  }
}
